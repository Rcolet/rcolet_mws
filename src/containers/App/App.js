import React, { Component } from 'react';
import style from './App.module.css';
import Header from '../../components/Header';
import PersonnalDetails from '../../components/PersonnalDetails';
import WorkExperience from '../../components/WorkExperience';
import Skills from '../../components/Skills';
import Projects from '../../components/Projects';
import Other from '../../components/Other';

class App extends Component {
  render() {
    return (
      <div className={style.App}>
        <Header />
        <PersonnalDetails />
        <WorkExperience />
        <Skills />
        <Projects />
        <Other />
      </div>
    );
  }
}

export default App;
